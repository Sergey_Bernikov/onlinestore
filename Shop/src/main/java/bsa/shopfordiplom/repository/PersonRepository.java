package bsa.shopfordiplom.repository;

import bsa.shopfordiplom.models.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Transactional
@Repository
public interface PersonRepository extends JpaRepository <Person, Long> {
    Person findAllByPhoneNumber(String phoneNumber);    // поиск пользователя (клиента) по номеру телефона
    void deleteAllByPhoneNumber(String phoneNumber);    // удаление пользователя (клиента) по номеру телефона
}

