package bsa.shopfordiplom.models;

import lombok.*;
import javax.persistence.*;
@AllArgsConstructor                                         // создать конструктор со всеми полями
@NoArgsConstructor                                          // создать пустой конструктор
@Builder                                                    // объект-строитель
@Getter                                                     // создать геттеры
@Setter                                                     // создать сеттеры
@ToString                                                   // реализация метода toString()

@Entity                                                     // @Entity - указывает, что данный бин (класс) является сущностью (класс будет содержать таблицу)
public class Person {                   // Пользователь (клиент)
        @Id                                                     // @Id — id колонки
        @GeneratedValue(strategy = GenerationType.IDENTITY)     // стратегия IDENTITY позволяет надежно получать уникальные ID вне зависимости от клиентов, использующих БД
        @Column(name = "id")
        private long id;                                        // уникальный id
        @Column(name = "name")
        private String name;                                    // имя
        @Column(name = "phonenumber")
        private String phoneNumber;                             // номер телефона
        @Column(name = "email")
        private String email;                                   // email
}







/*
        @NotEmpty (message = "Поле не должно быть пустым")                             // правило валидации (поле не может быть пустым)
        @Size (min = 2, max = 20, message = "Введите количество символов от 2 до 20")  // правило валидации (диапазон ввода символов (текста))
        @Column(name = "name")
        private String name;                                                           // имя

        @NotEmpty (message = "Поле не должно быть пустым")                             // правило валидации (поле не может быть пустым)
        @Size (min = 5, max = 15, message = "Введите количество символов от 2 до 15")  // правило валидации (диапазон ввода символов(текста))
        @Min(value = 0, message = "Число должно быть положительное")
        @Column(name = "phoneNumber")
        private String phoneNumber;                                                      // номер телефона

        @NotEmpty (message = "Поле не должно быть пустым")                             // правило валидации (поле не может быть пустым)
        @Email (message = "Email не валидный")                                         // правило валидации (проверка email при помощи регулярных выражений - формат)
        @Column(name = "email")
        private String email;                                                          // email


                        <!--        Валидация моделей (проверка)            -->

<!-- https://mvnrepository.com/artifact/org.hibernate.validator/hibernate-validator -->
<dependency>
<groupId>org.hibernate.validator</groupId>
<artifactId>hibernate-validator</artifactId>
<version>8.0.0.Final</version>
</dependency>                                                                                           */