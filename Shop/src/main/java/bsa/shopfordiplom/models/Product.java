package bsa.shopfordiplom.models;

import lombok.*;
import javax.persistence.*;
@AllArgsConstructor                                         // создать конструктор со всеми полями
@NoArgsConstructor                                          // создать пустой конструктор
@Builder                                                    // объект-строитель
@Getter                                                     // создать геттеры
@Setter                                                     // создать сеттеры
@ToString                                                   // реализация метода toString()

@Entity                                                     // @Entity - указывает, что данный бин (класс) является сущностью (класс будет содержать таблицу)
public class Product {          // Товар
    @Id                                                     // @Id — id колонки
    @GeneratedValue(strategy = GenerationType.IDENTITY)     // стратегия IDENTITY позволяет надежно получать уникальные ID вне зависимости от клиентов, использующих БД
    @Column(name = "id")
    private long id;                                        // уникальный id
    @Column(name = "articlenumber")
    private long articleNumber;                             // артикул товара
    @Column(name = "name")
    private String name;                                    // наименование товара
    @Column(name = "price")
    private double price;                                   // цена товара
}
