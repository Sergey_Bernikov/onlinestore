package bsa.shopfordiplom.controllers;

import bsa.shopfordiplom.methodClass.ProductMethodsWithABasket;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class Cart {
    private final ProductMethodsWithABasket productMethodsWithABasket;
    @Autowired
    public Cart(ProductMethodsWithABasket productMethodsWithABasket) {
        this.productMethodsWithABasket = productMethodsWithABasket;
    }
    @GetMapping("/cart")
    String shoppingCart(Model model){
        model.addAttribute("countCart", ProductMethodsWithABasket.count);
        model.addAttribute("totalPrice", ProductMethodsWithABasket.totalPrice);
        model.addAttribute("productsInTheBasket",  productMethodsWithABasket.allProductsInTheBasket());
        return "cart";
    }
}
