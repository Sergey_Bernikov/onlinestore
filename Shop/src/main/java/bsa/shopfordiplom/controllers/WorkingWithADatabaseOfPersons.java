package bsa.shopfordiplom.controllers;

import bsa.shopfordiplom.models.Person;
import bsa.shopfordiplom.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import java.sql.SQLException;

            // Работа через JPA

@Controller
@RequestMapping("/workingWithADatabaseOfPersons")
public class WorkingWithADatabaseOfPersons {          // контроллер для странички сайта "Работа с базой данных пользователей (клиентов)"
    @Autowired
    PersonRepository personRepository;
    // *********************************************** просмотр базы данных с пользователями (клиентами) ***************
    @GetMapping("/viewingThePersonDatabase")
    public String viewingThePersonDatabase(Model model) {
        model.addAttribute("viewingThePersonDatabase", personRepository.findAll());
        return "workingWithADatabaseOfPersons/viewingThePersonDatabase";
    }
    // *********************************************** добавить пользователя (клиента) в БД (GET) **********************
    @GetMapping("/addAPersonToTheDatabase")
    public String addAPersonToTheDatabase(){
        return "workingWithADatabaseOfPersons/addAPersonToTheDatabase";
    }
    // *********************************************** добавить ользователя (клиента) в БД (POST) **********************
    @PostMapping("/addAPersonToTheDatabase")
    public String addAPersonToTheDatabase(@RequestParam("name") String name, @RequestParam ("phoneNumber") String phoneNumber,
                                          @RequestParam ("email") String email) throws SQLException {
        if (name.equals("")||phoneNumber.equals("")||email.equals("")) return "/workingWithADatabaseOfPersons/addAPersonToTheDatabase";
        Person person = Person.builder()
                .name(name)
                .phoneNumber(phoneNumber)
                .email(email)
                .build();
        personRepository.save(person);
        return "/admin";
    }
    // *********************************************** удалить пользователя (клиента) из БД по номеру телефона (GET) ***
    @GetMapping("/removeAPersonToTheDatabase")
    public String removeAProductToTheDatabase(){
        return "workingWithADatabaseOfPersons/removeAPersonToTheDatabase";
    }
    // *********************************************** удалить пользователя (клиента) из БД по номеру телефона (POST) **
    @PostMapping("/removeAPersonToTheDatabase")
    public String removeAProductToTheDatabase(@RequestParam("phoneNumber") String phoneNumber) throws SQLException {
        if (phoneNumber.equals("")) return "workingWithADatabaseOfPersons/removeAPersonToTheDatabase";
        personRepository.deleteAllByPhoneNumber(phoneNumber);
        return "workingWithADatabaseOfPersons/removeAPersonToTheDatabase";
        //return "/admin";
    }
    // *********************************************** поиск пользователя (клиента) в БД по номеру телефона (GET) ******
    @GetMapping("/searchForACustomerInTheDatabaseByPhoneNumber")
    public String searchForACustomerInTheDatabaseByPhoneNumber(){
        return "workingWithADatabaseOfPersons/searchForACustomerInTheDatabaseByPhoneNumber";
    }
    // *********************************************** поиск пользователя (клиента) в БД по номеру телефона (POST) *****
    @PostMapping ("/searchForACustomerInTheDatabaseByPhoneNumber")
    public String searchForACustomerInTheDatabaseByPhoneNumber(@RequestParam("phoneNumber") String phoneNumber, Model model) throws SQLException {
        if (phoneNumber.equals("")) return "workingWithADatabaseOfPersons/searchForACustomerInTheDatabaseByPhoneNumber";

        model.addAttribute("personSearch", personRepository.findAllByPhoneNumber(phoneNumber));
        return "workingWithADatabaseOfPersons/theResultOfTheCustomersSearchInTheDatabaseByPhoneNumber";
    }
}
