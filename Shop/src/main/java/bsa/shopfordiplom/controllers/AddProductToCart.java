package bsa.shopfordiplom.controllers;

import bsa.shopfordiplom.methodClass.ProductMethodsWithABasket;
import bsa.shopfordiplom.models.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class AddProductToCart {
    private final ProductMethodsWithABasket productMethodsWithABasket;
    @Autowired
    public AddProductToCart(ProductMethodsWithABasket productMethodsWithABasket) {
        this.productMethodsWithABasket = productMethodsWithABasket;
    }
    @GetMapping("/addProductToCart")
    public String addProduct (@RequestParam(value = "articleNumber", required = false) long articleNumber,
                              @RequestParam(value = "name", required = false) String name,
                              @RequestParam(value = "price", required = false) double price,
                              Model model) {
        Product product = Product.builder()
                .articleNumber(articleNumber)
                .name(name)
                .price(price)
                .build();
        model.addAttribute("count", productMethodsWithABasket.addProductInTheBasket(product));
        return "redirect:/home";
    }
}
