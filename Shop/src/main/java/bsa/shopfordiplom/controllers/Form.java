package bsa.shopfordiplom.controllers;

import bsa.shopfordiplom.methodClass.PersonMethods;
import bsa.shopfordiplom.methodClass.ProductMethodsWithABasket;
import bsa.shopfordiplom.models.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import java.sql.SQLException;

@Controller
public class Form {
    private final PersonMethods personMethods;
    private final ProductMethodsWithABasket productMethodsWithABasket;
    @Autowired
    public Form(PersonMethods methods, ProductMethodsWithABasket productMethodsWithABasket) {
        this.personMethods = methods;
        this.productMethodsWithABasket = productMethodsWithABasket;
    }
    @GetMapping("/form")
    public String newPerson(Model model){
        model.addAttribute("countCart", ProductMethodsWithABasket.count);
        return "form";
    }
    @PostMapping ("/form")
    public String createPersonNew(@RequestParam("name") String name, @RequestParam ("phoneNumber") String phoneNumber,
                                  @RequestParam ("email") String email) throws SQLException {
        if (name.equals("")||phoneNumber.equals("")||email.equals("")) return "/form";
        Person person = Person.builder()
                .name(name)
                .phoneNumber(phoneNumber)
                .email(email)
                .build();
        personMethods.createPerson(person);
        productMethodsWithABasket.removeALLProductInTheBasket();                        // удалить весь товар из корзины
        ProductMethodsWithABasket.count = 0;                                            // обнулить счетчик товаров
        ProductMethodsWithABasket.totalPrice = 0;                                       // обнулить общую сумму за товар
        return "thanksForThePurchase";
    }
 }
