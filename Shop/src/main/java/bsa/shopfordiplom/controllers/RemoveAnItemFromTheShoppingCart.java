package bsa.shopfordiplom.controllers;

import bsa.shopfordiplom.methodClass.ProductMethodsWithABasket;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class RemoveAnItemFromTheShoppingCart {
    private final ProductMethodsWithABasket productMethodsWithABasket;
    @Autowired
    public RemoveAnItemFromTheShoppingCart(ProductMethodsWithABasket productMethodsWithABasket) {
        this.productMethodsWithABasket = productMethodsWithABasket;
    }
    @GetMapping("/removeProduct")
        public String removeProduct (@RequestParam(value = "id", required = false) long id) {
            productMethodsWithABasket.removeProductInTheBasket(id);
            return "redirect:/cart";
        }
}
