package bsa.shopfordiplom.controllers;

import bsa.shopfordiplom.methodClass.ProductMethods;
import bsa.shopfordiplom.models.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import java.sql.SQLException;

            // Работа через JDBC

@Controller
@RequestMapping("/workingWithTheProductDatabase")
public class WorkingWithTheProductDatabase {            // контроллер для странички сайта "Работа с базой данных товаров"
    private final ProductMethods productMethods;
    @Autowired
    public WorkingWithTheProductDatabase(ProductMethods productMethods) {
        this.productMethods = productMethods;
    }
    // *********************************************** просмотр базы данных с товаром **********************************
    @GetMapping("/viewingTheProductDatabase")
    public String viewingTheProductDatabase(Model model) throws SQLException {
        model.addAttribute("viewingTheProductDatabase", productMethods.showProduct());
        return "workingWithTheProductDatabase/viewingTheProductDatabase";
    }
    // *********************************************** добавить товар в БД (GET) ***************************************
    @GetMapping("/addAProductToTheDatabase")
    public String addAProductToTheDatabase(){
        return "workingWithTheProductDatabase/addAProductToTheDatabase";
    }
    // *********************************************** добавить товар в БД (POST) **************************************
    @PostMapping("/addAProductToTheDatabase")
    public String addAProductToTheDatabase(@RequestParam("articleNumber") String articleNumbers,
                                           @RequestParam ("name") String name,
                                           @RequestParam ("price") String prices) throws SQLException {

        if (articleNumbers.equals("")||name.equals("")||prices.equals("")) return "workingWithTheProductDatabase/addAProductToTheDatabase";

        long articleNumber = Long.parseLong(articleNumbers);
        double price = Double.parseDouble(prices);

        Product product = Product.builder()
                .articleNumber(articleNumber)
                .name(name)
                .price(price)
                .build();
        productMethods.createProduct(product);
        return "/admin";
    }
    // *********************************************** удалить товар из БД по артикулу (GET) ***************************
    @GetMapping("/removeAProductToTheDatabase")
    public String removeAProductToTheDatabase(){
        return "workingWithTheProductDatabase/removeAProductToTheDatabase";
    }
    // *********************************************** удалить товар из БД по артикулу (POST) **************************
    @PostMapping("/removeAProductToTheDatabase")
    public String removeAProductToTheDatabase(@RequestParam("articleNumber") String articleNumbers) throws SQLException {
        if (articleNumbers.equals("")) return "workingWithTheProductDatabase/removeAProductToTheDatabase";
        long articleNumber = Long.parseLong(articleNumbers);
        productMethods.removeProduct(articleNumber);
        return "/admin";
    }
    // *********************************************** поиск товара в БД по артикулу (GET) ***************************
    @GetMapping("/searchForProductsByArticle")
    public String searchForProductsByArticle(){
        return "workingWithTheProductDatabase/searchForProductsByArticle";
    }
    // *********************************************** поиск товара в БД по артикулу (POST) ***************************
    @PostMapping ("/searchForProductsByArticle")
    public String searchForProductsByArticle(@RequestParam("articleNumber") String articleNumbers, Model model) throws SQLException {
        if (articleNumbers.equals("")) return "workingWithTheProductDatabase/searchForProductsByArticle";
        long articleNumber = Long.parseLong(articleNumbers);
        model.addAttribute("productSearch", productMethods.searchForProductsByArticle(articleNumber));
        return "workingWithTheProductDatabase/productSearchResultInTheDatabaseByArticle";
    }
}
