package bsa.shopfordiplom.controllers;

import bsa.shopfordiplom.methodClass.ProductMethodsWithABasket;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class Admin {
    @GetMapping("/admin")
    String admin(Model model){
        model.addAttribute("countCart", ProductMethodsWithABasket.count);
        return "admin";
    }
}
