package bsa.shopfordiplom.controllers;

import bsa.shopfordiplom.methodClass.ProductMethods;
import bsa.shopfordiplom.methodClass.ProductMethodsWithABasket;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import java.sql.SQLException;

@Controller
public class Home {
    private final ProductMethods productMethods;
    @Autowired
    public Home(ProductMethods productMethods) {
        this.productMethods = productMethods;
    }
    @GetMapping("/home") // в аннотации @GetMapping, указываем какой URL ("/"), будет приходить в метод (в данном случае - метод home)
    public String home(Model model) throws SQLException {                       // получим список товаров из БД через List
        model.addAttribute("products",  productMethods.showProduct());
        model.addAttribute("countCart", ProductMethodsWithABasket.count);
        return "home";
    }
}



