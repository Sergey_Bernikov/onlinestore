package bsa.shopfordiplom.controllers;

import bsa.shopfordiplom.methodClass.PersonMethods;
import bsa.shopfordiplom.methodClass.ProductMethodsWithABasket;
import bsa.shopfordiplom.models.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import java.sql.SQLException;

@Controller
public class CheckPersonForAuthorization {
    private final PersonMethods personMethods;
    private final ProductMethodsWithABasket productMethodsWithABasket;
    @Autowired
    public CheckPersonForAuthorization(PersonMethods personMethods, ProductMethodsWithABasket productMethodsWithABasket) {
        this.personMethods = personMethods;
        this.productMethodsWithABasket = productMethodsWithABasket;
    }
    @GetMapping("/checkPersonForAuthorization")
    public String Authorization(Model model){
        model.addAttribute("countCart", ProductMethodsWithABasket.count);
        return "checkPersonForAuthorization";
    }
    @PostMapping("/checkPersonForAuthorization")
    public String createPersonNew(@RequestParam ("phoneNumber") String phoneNumber, Model model) throws SQLException {
        if (phoneNumber.equals("1111")) return "redirect:/admin";
        Person person = personMethods.showPersonByPhoneNumber(phoneNumber);
        if (person.getPhoneNumber() == null) return "warning";
        model.addAttribute("name", person.getName());
        productMethodsWithABasket.removeALLProductInTheBasket();                        // удалить весь товар из корзины
        ProductMethodsWithABasket.count = 0;                                            // обнулить счетчик товаров
        ProductMethodsWithABasket.totalPrice = 0;                                       // обнулить общую сумму за товар
        return "thanksForThePurchase";
    }
}
