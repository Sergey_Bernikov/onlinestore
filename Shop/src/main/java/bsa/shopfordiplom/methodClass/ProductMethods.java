package bsa.shopfordiplom.methodClass;

import bsa.shopfordiplom.models.Product;
import org.springframework.stereotype.Component;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
@Component                                    // spring создаст бин этого класса и данный бин будет внедрен в контроллер
public class ProductMethods {                 // класс с методами для работы с продуктами
    private final Connection connection;      // установка соединения с БД
    {
        try {
            connection = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/tablesfordiploma",
                    "root",
                    "root"
            );
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
    public void createProduct(Product product) throws SQLException {                      // добавить товар в БД product
        PreparedStatement preparedStatement = connection.prepareStatement("insert into product (id, articleNumber, name, price) values (?,?,?,?)");
        preparedStatement.setLong(1, product.getId());
        preparedStatement.setLong(2, product.getArticleNumber());
        preparedStatement.setString(3, product.getName());
        preparedStatement.setDouble(4, product.getPrice());
        preparedStatement.executeUpdate();
    }
    public List<Product> showProduct() throws SQLException {                         // показать весь товар в БД product
        PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM product");
        ResultSet resultSet = preparedStatement.executeQuery();

        List<Product> productInBD = new ArrayList<>();
        long id;
        long articleNumber;
        String name;
        double price;

        while (resultSet.next()) {
            id = resultSet.getLong("id");
            articleNumber = resultSet.getLong("articleNumber");
            name = resultSet.getString("name");
            price = resultSet.getDouble("price");

            Product productFromBDInList = Product.builder()
                    .articleNumber(articleNumber)
                    .name(name)
                    .price(price)
                    .build();

            productInBD.add(productFromBDInList);
        }
        return productInBD;
    }
    public void removeProduct(long articleNumber) throws SQLException {                 // удалить товар из БД product по артикулу
        PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM product WHERE articleNumber=?");
        preparedStatement.setLong(1, articleNumber);
        preparedStatement.executeUpdate();
    }
    public Product searchForProductsByArticle(long articleNumber) throws SQLException { // поиск товара в БД product по артикулу
        PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM product WHERE articleNumber=?");
        preparedStatement.setLong(1, articleNumber);
        ResultSet resultSet = preparedStatement.executeQuery();

        Product productFromBD = new Product();
        long article;
        String name;
        double price;

        while (resultSet.next()) {
            article = resultSet.getLong("articleNumber");
            name = resultSet.getString("name");
            price = resultSet.getDouble("price");

            productFromBD = Product.builder()
                    .articleNumber(article)
                    .name(name)
                    .price(price)
                    .build();
        }
        return productFromBD;
    }
}
