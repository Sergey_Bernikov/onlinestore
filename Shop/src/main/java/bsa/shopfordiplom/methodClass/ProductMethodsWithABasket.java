package bsa.shopfordiplom.methodClass;

import bsa.shopfordiplom.models.Product;
import org.springframework.stereotype.Component;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
@Component                                    // spring создаст бин этого класса и данный бин будет внедрен в контроллер
public class ProductMethodsWithABasket {      // класс с методами для работы с корзиной товаров
    static public int count;                                                            // счетчик для корзины в html (количество товаров в корзине)
    private long id;                                                                     // id товара
    public static double totalPrice;                                                     // общая сумма товаров
    private final List<Product> productsInTheBasket;                                     // корзина с товаром (список List)
   {
        productsInTheBasket = new ArrayList<>();
   }
    public List <Product> allProductsInTheBasket(){                                      // показать все товары в корзине
        return productsInTheBasket;
    }
    public long addProductInTheBasket(Product product){                                  // добавить товар в корзину
        count++;
        product.setId(id++);
        totalPrice+=product.getPrice();
        productsInTheBasket.add(product);
        return count;
    }
    public void removeALLProductInTheBasket(){                                          // удалить весь товар из корзины
       productsInTheBasket.clear();
    }
    public void removeProductInTheBasket(long id){                                       // удалить товар из корзины
        count--;
        Iterator<Product> productIterator = productsInTheBasket.iterator();
        while (productIterator.hasNext()){
            Product product = productIterator.next();
            if (product.getId() == id) {
                totalPrice-=product.getPrice();
                productIterator.remove();
            }
        }
    }
}
