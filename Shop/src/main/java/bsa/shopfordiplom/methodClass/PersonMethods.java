package bsa.shopfordiplom.methodClass;

import bsa.shopfordiplom.models.Person;
import org.springframework.stereotype.Component;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Component                                    // spring создаст бин этого класса и данный бин будет внедрен в контроллер
public class PersonMethods {                  // класс с методами для работы с пользователями (клиентами)
    private final Connection connection;      // установка соединения с БД
    {
        try {
            connection = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/tablesfordiploma",
                    "root",
                    "root"
            );
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
    public void createPerson(Person person) throws SQLException {                                          // добавить пользователя (клиента) в БД person
        PreparedStatement preparedStatement = connection.prepareStatement("insert into person (id,  name, phoneNumber, email) values (?,?,?,?)");
        preparedStatement.setLong(1, person.getId());
        preparedStatement.setString(2, person.getName());
        preparedStatement.setString(3, person.getPhoneNumber());
        preparedStatement.setString(4, person.getEmail());
        preparedStatement.executeUpdate();
        }
    public Person showPersonByPhoneNumber(String phoneNumberPerson) throws SQLException {                 // показать пользователя (клиента) в БД person по номеру телефона
        PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM person WHERE phoneNumber=?");
        preparedStatement.setString(1, phoneNumberPerson);
        ResultSet resultSet = preparedStatement.executeQuery();

        Person personFromBDInList = new Person();

        long id;
        String email;
        String name;
        String phoneNumber;

        while (resultSet.next()) {
            id = resultSet.getLong("id");
            email = resultSet.getString("email");
            name = resultSet.getString("name");
            phoneNumber = resultSet.getString("phoneNumber");

            personFromBDInList = Person.builder()
                    .id(id)
                    .email(email)
                    .name(name)
                    .phoneNumber(phoneNumber)
                    .build();
        }
        return personFromBDInList;
    }
    public List<Person> showPersonInBD() throws SQLException {                                          // показать всех пользователей (клиентов) в БД person
        PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM person");
        ResultSet resultSet = preparedStatement.executeQuery();

        List<Person> personInBD = new ArrayList<>();

        String email;
        String name;
        String phoneNumber;

        while (resultSet.next()) {
            email = resultSet.getString("email");
            name = resultSet.getString("name");
            phoneNumber = resultSet.getString("phoneNumber");

            Person person = Person.builder()
                    .email(email)
                    .name(name)
                    .phoneNumber(phoneNumber)
                    .build();

            personInBD.add(person);
        }
        return personInBD;
    }
    public void removePerson(long phoneNumber) throws SQLException {                                    // удалить пользователя (клиента) из БД person по номеру телефона
        PreparedStatement preparedStatement = connection.prepareStatement("DELETE FROM person WHERE phoneNumber=?");
        preparedStatement.setLong(1, phoneNumber);
        preparedStatement.executeUpdate();
    }
}

